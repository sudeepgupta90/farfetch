#solution 1: solved partially
"""
Brute Force Solution
steps
1) generate all permutations of a given string
2) find the next occuring permutation

This is neither space, nor time optimised
"""
def perms(A):
    """generates all permutations in lexical order"""

    li=[]
    if len(A)==0:
        return []

    if len(A)==1:
        return [A]

    for i in range(len(A)):
        x= A[i]
        rem= A[:i]+A[i+1:]

        for p in perms(rem):
            y= [x]+p
            li.append(y)

    return li


# print (perms([1,2,3,4]))

# solution2: used the property of permutations, how they are calculated/generated
def lexNext(A:str):
    """https://en.wikipedia.org/wiki/Permutation#Generation_in_lexicographic_order"""
    
    if len(A)<=1:
        return "not possible"   # base case: only 1 char
    if len(set(list(A)))==1:
        return "not possible"   # base case: can not create more combs
    
    i= len(A)-1

    while(A[i-1]>=A[i]) and i>0:    #find first character which can be swapped
        i-=1

    if i==0:
        return "not possible" #string is already sorted in descending order

    j= len(A)-1
    while(j>i and A[j]<=A[i-1]):
        j-=1

    A=list(A)

    A[i-1], A[j]= A[j], A[i-1]

    s1= "".join(A[:i])
    s2= A[i:]
    s2= "".join(s2[::-1]) #reverse s2

    return s1+s2

#test cases
for i in ["ab", "bb", "hefg", "dhck", "dkhc"]:
    print (lexNext(i))